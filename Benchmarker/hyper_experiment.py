import yaml
import matplotlib.pyplot as plt
import pandas as pd
import warnings
import numpy as np
import ast
from config import Label, Names
from utils import label_metrics

with warnings.catch_warnings():
    warnings.filterwarnings('ignore', category=DeprecationWarning)
    import seaborn as sns
    import h2o


class HyperExperiment(yaml.YAMLObject):
    yaml_tag = u"Experiment"

    @classmethod
    def load(cls, file):
        f = open(file, "r")
        e = yaml.load(f)
        f.close()
        return e

    def __init__(self, name, x, y, filename, train_ratio, validation_ratio, steps, optimizers):
        self.models = dict()
        self.name = name
        self.x = x
        self.y = y
        self.filename = filename
        self.train_ratio = train_ratio
        self.validation_ratio = train_ratio + validation_ratio
        self.steps = steps
        self.optimizers = optimizers

    def add(self, name, model):
        self.models[name] = model

    def execute(self):
        print("* Executing experiment >>{}<<".format(self.name))
        print("* Importing file >>{}<<".format(self.filename))
        fr = h2o.import_file(path=self.filename, na_strings=[])
        results = pd.DataFrame(columns=Names)
        r = fr[0].runif()
        train = fr[r < self.train_ratio]
        valid = fr[(self.train_ratio <= r) & (r < self.train_ratio + (self.validation_ratio - self.train_ratio))]
        test = fr[1.0 - (1.0 - self.validation_ratio) <= r]

        try:

            for _, modelCfg in self.models.items():
                print("")
                print(":::: Benchmarking {} model...".format(modelCfg.name))
                results = results.append(modelCfg.execute(self.name, self.x, self.y, train, valid, test, -1))

        finally:
            h2o.remove(fr)
            h2o.remove(train)
            h2o.remove(valid)
            h2o.remove(test)
        return results
