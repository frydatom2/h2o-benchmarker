
def switch(options, selected):
    return "\n          ".join(["<boolean>{}</boolean>".format("true" if x == selected else "false") for x in options])

def backPropagation(hidden_l1=5, hidden_l2=0, trainingCycles=600, acceptableError=0.0, learningRate=0.2,
                    momentum = 0.3, activationFunction = "sigmoid"):
    return """
<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>BackPropagation classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.neural.BackPropagationModelConfig>
      <classRef>game.models.single.neural.BackPropagationModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <firstLayerNeurons>{hidden_l1}</firstLayerNeurons>
      <secondLayerNeurons>{hidden_l2}</secondLayerNeurons>
      <trainingCycles>{trainingCycles}</trainingCycles>
      <acceptableError>{acceptableError}</acceptableError>
      <activationFunction>
        <elements class="string-array">
          <string>sigmoid</string>
          <string>sigmoid_offset</string>
        </elements>
        <elementEnabled>
          {activationFunction}
        </elementEnabled>
      </activationFunction>
      <learningRate>{learningRate}</learningRate>
      <momentum>{momentum}</momentum>
    </configuration.models.single.neural.BackPropagationModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>
    """.format(hidden_l1 = hidden_l1, hidden_l2 = hidden_l2, trainingCycles = trainingCycles,
               acceptableError = acceptableError, learningRate = learningRate, momentum = momentum,
               activationFunction=switch(["sigmoid", "sigmoid_offset"], activationFunction))


def cascadeCorrelation(acceptableError = 0.001, maxLayersNumber = 5, candNumber = 1, usedAlg = "Rprop",
                       activationFunction="sigmoid_offset"):
    return """
<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Cascade Correlation classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.neural.CascadeCorrelationModelConfig>
      <classRef>game.models.single.neural.CascadeCorrelationModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <acceptableError>{acceptableError}</acceptableError>
      <maxLayersNumber>{maxLayersNumber}</maxLayersNumber>
      <candNumber>{candNumber}</candNumber>
      <usedAlg>
        <elements class="string-array">
          <string>Quickprop</string>
          <string>Rprop</string>
        </elements>
        <elementEnabled>
          {usedAlg}
        </elementEnabled>
      </usedAlg>
      <activationFunction>
        <elements class="string-array">
          <string>sigmoid</string>
          <string>sigmoid_offset</string>
          <string>symmetric_sigmoid</string>
        </elements>
        <elementEnabled>
          {activationFunction}
        </elementEnabled>
      </activationFunction>
    </configuration.models.single.neural.CascadeCorrelationModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>
""".format(acceptableError = acceptableError, maxLayersNumber = maxLayersNumber, candNumber = candNumber,
           usedAlg=switch(["Quickprop", "Rprop"], usedAlg),
           activationFunction=switch(["sigmoid", "sigmoid_offset", "symmetric_sigmoid"], activationFunction))



def quickProp(hidden_l1 = 5, hidden_l2 = 0, trainingCycles=600, acceptableError=0.0, activationFunction="sigmoid",
              maxGrowthFactor=2.0, epsilon = 7.0e-4, splitEpsilon=False):
    return """
<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>QuickProp classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.neural.QuickpropModelConfig>
      <classRef>game.models.single.neural.QuickpropModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <firstLayerNeurons>{hidden_l1}</firstLayerNeurons>
      <secondLayerNeurons>{hidden_l2}</secondLayerNeurons>
      <trainingCycles>{trainingCycles}</trainingCycles>
      <acceptableError>{acceptableError}</acceptableError>
      <activationFunction>
        <elements class="string-array">
          <string>sigmoid</string>
          <string>sigmoid_offset</string>
          <string>symmetric_sigmoid</string>
        </elements>
        <elementEnabled>
          {activationFunction}
        </elementEnabled>
      </activationFunction>
      <maxGrowthFactor>{maxGrowthFactor}</maxGrowthFactor>
      <epsilon>{epsilon}</epsilon>
      <splitEpsilon>{splitEpsilon}</splitEpsilon>
    </configuration.models.single.neural.QuickpropModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>
    """.format(hidden_l1 = hidden_l1, hidden_l2 = hidden_l2, trainingCycles=trainingCycles,
               acceptableError=acceptableError, activationFunction=switch(["sigmoid", "sigmoid_offset",
                                                                            "symmetric_sigmoid"], activationFunction),
               maxGrowthFactor=maxGrowthFactor, epsilon=epsilon, splitEpsilon = splitEpsilon)


def rProp(hidden_l1 = 5, hidden_l2 = 0, trainingCycles=600, acceptableError=0.0, activationFunction="sigmoid",
          etaMinus=0.5, etaPlus=1.2):
    return """
<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Rprop classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.neural.RpropModelConfig>
      <classRef>game.models.single.neural.RpropModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <firstLayerNeurons>{hidden_l1}</firstLayerNeurons>
      <secondLayerNeurons>{hidden_l2}</secondLayerNeurons>
      <trainingCycles>{trainingCycles}</trainingCycles>
      <acceptableError>{acceptableError}</acceptableError>
      <activationFunction>
        <elements class="string-array">
          <string>sigmoid</string>
          <string>sigmoid_offset</string>
          <string>symmetric_sigmoid</string>
        </elements>
        <elementEnabled>
          {activationFunction}
        </elementEnabled>
      </activationFunction>
      <etaMinus>{etaMinus}</etaMinus>
      <etaPlus>{etaPlus}</etaPlus>
    </configuration.models.single.neural.RpropModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>
    """.format(hidden_l1 = hidden_l1, hidden_l2 = hidden_l2, trainingCycles=trainingCycles,
               acceptableError=acceptableError, activationFunction=switch(["sigmoid", "sigmoid_offset",
                                                                            "symmetric_sigmoid"], activationFunction),
               etaMinus=etaMinus, etaPlus=etaPlus)
               
               
def treeEvolution(time=300):
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Tree Evolution Model</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.evolution.TreeEvolutionModelConfig>
      <classRef>game.models.evolution.TreeEvolutionModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <computationTime>{}</computationTime>
    </configuration.models.evolution.TreeEvolutionModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>""".format(time)

def sine():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Sine classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.SineModelConfig>
      <classRef>game.models.single.SineModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.SineModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def sineNorm():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Sine Norm classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.SineNormModelConfig>
      <classRef>game.models.single.SineNormModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.SineNormModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def sigmoid():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Sigmoid classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.SigmoidModelConfig>
      <classRef>game.models.single.SigmoidModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.SigmoidModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def sigmoidNorm():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Sigmoid Norm classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.SigmoidNormModelConfig>
      <classRef>game.models.single.SigmoidNormModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.SigmoidNormModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def poly():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Polynomial classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.PolynomialModelConfig>
      <classRef>game.models.single.PolynomialModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
      <maxDegree>2</maxDegree>
    </configuration.models.single.PolynomialModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def linear():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Linear classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.LinearModelConfig>
      <classRef>game.models.single.LinearModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
      <retrainWhenLmsFails>false</retrainWhenLmsFails>
    </configuration.models.single.LinearModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def gauss():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Gaussian classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.GaussianModelConfig>
      <classRef>game.models.single.GaussianModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.GaussianModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def gaussNorm():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Gaussian Norm classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.GaussianNormModelConfig>
      <classRef>game.models.single.GaussianNormModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.GaussianNormModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""

def gaussMulti():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>Gaussian Multi classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>UNIFORM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.single.GaussianMultiModelConfig>
      <classRef>game.models.single.GaussianMultiModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <trainerClassName>QuasiNewtonTrainer</trainerClassName>
      <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
        <rec>10</rec>
        <draw>10</draw>
        <forceAnalyticHessian>false</forceAnalyticHessian>
      </trainerCfg>
      <validationPercent>30</validationPercent>
      <validationEnabled>true</validationEnabled>
    </configuration.models.single.GaussianMultiModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""


def airlinesEnsemble1(n=5, m=5):
    return """<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>test classifier</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>{}</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierBoostingConfig>
      <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>{}</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.ClassifierModelConfig>
          <classRef>game.classifiers.single.ClassifierModel</classRef>
          <description>Exponencial classifier</description>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <baseModelsDef>UNIFORM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.SigmoidNormModelConfig>
              <classRef>game.models.single.SigmoidNormModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <trainerCfg class="configuration.game.trainers.QuasiNewtonConfig">
                <rec>10</rec>
                <draw>10</draw>
                <forceAnalyticHessian>false</forceAnalyticHessian>
              </trainerCfg>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
            </configuration.models.single.SigmoidNormModelConfig>
            <configuration.models.single.SigmoidNormModelConfig reference="../configuration.models.single.SigmoidNormModelConfig"/>
          </baseModelCfgs>
        </configuration.classifiers.single.ClassifierModelConfig>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierBoostingConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>""".format(n,m)

def CCGP_9CM_BRTM_8GM_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>CascadeGenProb{9x ClassifierModel{&lt;outputs&gt;x BoostingRTModel(tr=0.1)[8xGaussianModel]}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>9</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.ClassifierModelConfig>
      <classRef>game.classifiers.single.ClassifierModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.ensemble.BoostingRTModelConfig>
          <classRef>game.models.ensemble.ModelBoostingRT</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <modelsNumber>8</modelsNumber>
          <baseModelsDef>RANDOM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.GaussianModelConfig>
              <classRef>game.models.single.GaussianModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
            </configuration.models.single.GaussianModelConfig>
          </baseModelCfgs>
          <threshold>0.1</threshold>
        </configuration.models.ensemble.BoostingRTModelConfig>
      </baseModelCfgs>
    </configuration.classifiers.single.ClassifierModelConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>"""

def CAC_6CM_DM_PM_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierArbitratingConfig>
  <classRef>game.classifiers.ensemble.ClassifierArbitrating</classRef>
  <description>ClassifierArbitrating{6x ClassifierModel{&lt;outputs&gt;x DivideModel(mult=6.68)[7xPolynomialModel(degree=3)]}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>6</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.ClassifierModelConfig>
      <classRef>game.classifiers.single.ClassifierModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.ensemble.DivideModelConfig>
          <classRef>game.models.ensemble.ModelDivide</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <modelsNumber>7</modelsNumber>
          <baseModelsDef>RANDOM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.PolynomialModelConfig>
              <classRef>game.models.single.PolynomialModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
              <maxDegree>3</maxDegree>
            </configuration.models.single.PolynomialModelConfig>
          </baseModelCfgs>
          <clusterSizeMultiplier>6.68</clusterSizeMultiplier>
        </configuration.models.ensemble.DivideModelConfig>
      </baseModelCfgs>
    </configuration.classifiers.single.ClassifierModelConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierArbitratingConfig>"""

def CCGP_8B_2CM_EM_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>CascadeGenProb{8x Boosting{2x ClassifierModel{&lt;outputs&gt;x ExpModel}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>8</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierBoostingConfig>
      <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>2</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.ClassifierModelConfig>
          <classRef>game.classifiers.single.ClassifierModel</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <baseModelsDef>RANDOM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.ExpModelConfig>
              <classRef>game.models.single.ExpModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
            </configuration.models.single.ExpModelConfig>
          </baseModelCfgs>
        </configuration.classifiers.single.ClassifierModelConfig>
        <configuration.classifiers.single.ClassifierModelConfig reference="../configuration.classifiers.single.ClassifierModelConfig"/>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierBoostingConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>"""

def CA_4CM_PM_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierArbitratingConfig>
  <classRef>game.classifiers.ensemble.ClassifierArbitrating</classRef>
  <description>ClassifierArbitrating{4x ClassifierModel{&lt;outputs&gt;x PolynomialModel(degree=2)}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>4</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.ClassifierModelConfig>
      <classRef>game.classifiers.single.ClassifierModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.single.PolynomialModelConfig>
          <classRef>game.models.single.PolynomialModel</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <trainerClassName>QuasiNewtonTrainer</trainerClassName>
          <validationPercent>30</validationPercent>
          <validationEnabled>true</validationEnabled>
          <maxDegree>2</maxDegree>
        </configuration.models.single.PolynomialModelConfig>
      </baseModelCfgs>
    </configuration.classifiers.single.ClassifierModelConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierArbitratingConfig>"""


def CB_8CB_8DT_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierBoostingConfig>
  <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
  <description>TEST</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>8</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierBoostingConfig>
      <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>8</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.DecisionTreeClassifierConfig>
          <classRef>game.classifiers.single.DecisionTreeClassifier</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
        </configuration.classifiers.single.DecisionTreeClassifierConfig>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierBoostingConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierBoostingConfig>"""


def CBAG_40DT_Ensemble(n=40):
    return """<configuration.classifiers.ensemble.ClassifierBaggingConfig>
  <classRef>game.classifiers.ensemble.ClassifierBagging</classRef>
  <description>CBAG_{n}DT</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>{n}</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.DecisionTreeClassifierConfig>
      <classRef>game.classifiers.single.DecisionTreeClassifier</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
    </configuration.classifiers.single.DecisionTreeClassifierConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierBaggingConfig>""".format(n=n)

def CBAG_100DT_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierBaggingConfig>
  <classRef>game.classifiers.ensemble.ClassifierBagging</classRef>
  <description>TEST</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>100</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.DecisionTreeClassifierConfig>
      <classRef>game.classifiers.single.DecisionTreeClassifier</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
    </configuration.classifiers.single.DecisionTreeClassifierConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierBaggingConfig>"""

def CB_20CB_30DT_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierBoostingConfig>
  <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
  <description>TEST</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>20</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierBoostingConfig>
      <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>30</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.DecisionTreeClassifierConfig>
          <classRef>game.classifiers.single.DecisionTreeClassifier</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
        </configuration.classifiers.single.DecisionTreeClassifierConfig>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierBoostingConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierBoostingConfig>"""


def CGP_5CGP_3KNN_Ensemble():
    return """<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>CascadeGenProb{5x CascadeGenProb{3x KNN(k=9,vote=true,measure=CosineSimilarity)}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>5</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
      <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>3</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.KNNClassifierConfig>
          <classRef>game.classifiers.single.KNNClassifier</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <nearestNeighbours>9</nearestNeighbours>
          <weightedVote>true</weightedVote>
          <measureType>
            <elements class="string-array">
              <string>EuclideanDistance</string>
              <string>CamberraDistance</string>
              <string>ChebychevDistance</string>
              <string>CorrelationSimilarity</string>
              <string>CosineSimilarity</string>
              <string>DiceSimilarity</string>
              <string>DynamicTimeWarpingDistance</string>
              <string>InnerProductSimilarity</string>
              <string>JaccardSimilarity</string>
              <string>ManhattanDistance</string>
              <string>MaxProductSimilarity</string>
              <string>OverlapSimilarity</string>
            </elements>
            <elementEnabled>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>true</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
            </elementEnabled>
          </measureType>
        </configuration.classifiers.single.KNNClassifierConfig>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>"""

def wine_ensemble():
    return """
<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>CascadeGenProb{9x ClassifierModel{&lt;outputs&gt;x BoostingRTModel(tr=0.1)[8x GaussianModel]}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>9</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.ClassifierModelConfig>
      <classRef>game.classifiers.single.ClassifierModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.ensemble.BoostingRTModelConfig>
          <classRef>game.models.ensemble.ModelBoostingRT</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <modelsNumber>8</modelsNumber>
          <baseModelsDef>RANDOM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.GaussianModelConfig>
              <classRef>game.models.single.GaussianModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
            </configuration.models.single.GaussianModelConfig>
          </baseModelCfgs>
          <threshold>0.1</threshold>
        </configuration.models.ensemble.BoostingRTModelConfig>
      </baseModelCfgs>
    </configuration.classifiers.single.ClassifierModelConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>"""

def vehicle_ensemble():
    return """
<configuration.classifiers.ensemble.ClassifierArbitratingConfig>
  <classRef>game.classifiers.ensemble.ClassifierArbitrating</classRef>
  <description>ClassifierArbitrating{6x ClassifierModel{&lt;outputs&gt;x DivideModel(mult=6.68)[7x PolynomialModel(degree=3)]}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>6</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.ClassifierModelConfig>
      <classRef>game.classifiers.single.ClassifierModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.ensemble.DivideModelConfig>
          <classRef>game.models.ensemble.ModelDivide</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <modelsNumber>7</modelsNumber>
          <baseModelsDef>RANDOM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.PolynomialModelConfig>
              <classRef>game.models.single.PolynomialModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
              <maxDegree>3</maxDegree>
            </configuration.models.single.PolynomialModelConfig>
          </baseModelCfgs>
          <clusterSizeMultiplier>6.68</clusterSizeMultiplier>
        </configuration.models.ensemble.DivideModelConfig>
      </baseModelCfgs>
    </configuration.classifiers.single.ClassifierModelConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierArbitratingConfig>"""

def texture2_ensemble():
    return """<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>CascadeGenProb{8x Boosting{2x ClassifierModel{&lt;outputs&gt;x ExpModel}}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>8</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierBoostingConfig>
      <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>2</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.ClassifierModelConfig>
          <classRef>game.classifiers.single.ClassifierModel</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <baseModelsDef>RANDOM</baseModelsDef>
          <baseModelCfgs>
            <configuration.models.single.ExpModelConfig>
              <classRef>game.models.single.ExpModel</classRef>
              <maxLearningVectors>-1</maxLearningVectors>
              <maxInputsNumber>-1</maxInputsNumber>
              <trainerClassName>QuasiNewtonTrainer</trainerClassName>
              <validationPercent>30</validationPercent>
              <validationEnabled>true</validationEnabled>
            </configuration.models.single.ExpModelConfig>
          </baseModelCfgs>
        </configuration.classifiers.single.ClassifierModelConfig>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierBoostingConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>"""

def texture1_ensemble():
    return """<configuration.classifiers.ensemble.ClassifierArbitratingConfig>
  <classRef>game.classifiers.ensemble.ClassifierArbitrating</classRef>
  <description>ClassifierArbitrating{4x ClassifierModel{&lt;outputs&gt;x PolynomialModel(degree=2)}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>4</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.ClassifierModelConfig>
      <classRef>game.classifiers.single.ClassifierModel</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.single.PolynomialModelConfig>
          <classRef>game.models.single.PolynomialModel</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <trainerClassName>QuasiNewtonTrainer</trainerClassName>
          <validationPercent>30</validationPercent>
          <validationEnabled>true</validationEnabled>
          <maxDegree>2</maxDegree>
        </configuration.models.single.PolynomialModelConfig>
      </baseModelCfgs>
    </configuration.classifiers.single.ClassifierModelConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierArbitratingConfig>"""

def spirals3plus_ensemble():
    return """<configuration.classifiers.single.KNNClassifierConfig>
  <classRef>game.classifiers.single.KNNClassifier</classRef>
  <description>KNN(k=3,vote=true,measure=EuclideanDistance)</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <nearestNeighbours>3</nearestNeighbours>
  <weightedVote>true</weightedVote>
  <measureType>
    <elements class="string-array">
      <string>EuclideanDistance</string>
      <string>CamberraDistance</string>
      <string>ChebychevDistance</string>
      <string>CorrelationSimilarity</string>
      <string>CosineSimilarity</string>
      <string>DiceSimilarity</string>
      <string>DynamicTimeWarpingDistance</string>
      <string>InnerProductSimilarity</string>
      <string>JaccardSimilarity</string>
      <string>ManhattanDistance</string>
      <string>MaxProductSimilarity</string>
      <string>OverlapSimilarity</string>
    </elements>
    <elementEnabled>
      <boolean>true</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
    </elementEnabled>
  </measureType>
</configuration.classifiers.single.KNNClassifierConfig>"""



def spirals_bad_ensemble():
    return """<configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierCascadeGenProb</classRef>
  <description>CascadeGenProb{8x ClassifierArbitrating{4x KNN(k=3,vote=false,measure=MixedEuclideanDistance)}}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>8</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.ensemble.ClassifierArbitratingConfig>
      <classRef>game.classifiers.ensemble.ClassifierArbitrating</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <classifiersNumber>4</classifiersNumber>
      <baseClassifiersDef>RANDOM</baseClassifiersDef>
      <baseClassifiersCfgs>
        <configuration.classifiers.single.KNNClassifierConfig>
          <classRef>game.classifiers.single.KNNClassifier</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <nearestNeighbours>3</nearestNeighbours>
          <weightedVote>false</weightedVote>
          <measureType>
            <elements class="string-array">
              <string>EuclideanDistance</string>
              <string>CamberraDistance</string>
              <string>ChebychevDistance</string>
              <string>CorrelationSimilarity</string>
              <string>CosineSimilarity</string>
              <string>DiceSimilarity</string>
              <string>DynamicTimeWarpingDistance</string>
              <string>InnerProductSimilarity</string>
              <string>JaccardSimilarity</string>
              <string>ManhattanDistance</string>
              <string>MaxProductSimilarity</string>
              <string>OverlapSimilarity</string>
            </elements>
            <elementEnabled>
              <boolean>true</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
              <boolean>false</boolean>
            </elementEnabled>
          </measureType>
        </configuration.classifiers.single.KNNClassifierConfig>
      </baseClassifiersCfgs>
    </configuration.classifiers.ensemble.ClassifierArbitratingConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierCascadeGenProbConfig>"""



def spamBase_ensemble():
    return """<configuration.classifiers.single.ClassifierModelConfig>
  <classRef>game.classifiers.single.ClassifierModel</classRef>
  <description>ClassifierModel{&lt;outputs&gt;x CascadeGenModel[9x SigmoidModel]}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <baseModelsDef>RANDOM</baseModelsDef>
  <baseModelCfgs>
    <configuration.models.ensemble.CascadeGenModelConfig>
      <classRef>game.models.ensemble.ModelCascadeGen</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <modelsNumber>9</modelsNumber>
      <baseModelsDef>RANDOM</baseModelsDef>
      <baseModelCfgs>
        <configuration.models.single.SigmoidModelConfig>
          <classRef>game.models.single.SigmoidModel</classRef>
          <maxLearningVectors>-1</maxLearningVectors>
          <maxInputsNumber>-1</maxInputsNumber>
          <trainerClassName>QuasiNewtonTrainer</trainerClassName>
          <validationPercent>30</validationPercent>
          <validationEnabled>true</validationEnabled>
        </configuration.models.single.SigmoidModelConfig>
      </baseModelCfgs>
    </configuration.models.ensemble.CascadeGenModelConfig>
  </baseModelCfgs>
</configuration.classifiers.single.ClassifierModelConfig>"""


def segment_ensemble():
    return """<configuration.classifiers.ensemble.ClassifierBoostingConfig>
  <classRef>game.classifiers.ensemble.ClassifierBoosting</classRef>
  <description>Boosting{17x DecisionTree(maxdepth=24,conf=0.082,alt=0)}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>17</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.DecisionTreeClassifierConfig>
      <classRef>game.classifiers.single.DecisionTreeClassifier</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
    </configuration.classifiers.single.DecisionTreeClassifierConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierBoostingConfig>"""

def pendigits_ensemble():
    return """<configuration.classifiers.single.KNNClassifierConfig>
  <classRef>game.classifiers.single.KNNClassifier</classRef>
  <description>KNN(k=3,vote=false,measure=CosineSimilarity)</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <nearestNeighbours>3</nearestNeighbours>
  <weightedVote>false</weightedVote>
  <measureType>
    <elements class="string-array">
      <string>EuclideanDistance</string>
      <string>CamberraDistance</string>
      <string>ChebychevDistance</string>
      <string>CorrelationSimilarity</string>
      <string>CosineSimilarity</string>
      <string>DiceSimilarity</string>
      <string>DynamicTimeWarpingDistance</string>
      <string>InnerProductSimilarity</string>
      <string>JaccardSimilarity</string>
      <string>ManhattanDistance</string>
      <string>MaxProductSimilarity</string>
      <string>OverlapSimilarity</string>
    </elements>
    <elementEnabled>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>true</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
    </elementEnabled>
  </measureType>
</configuration.classifiers.single.KNNClassifierConfig>"""

def ionosphere_bad_ensemble():
    return """<configuration.classifiers.single.DecisionTreeClassifierConfig>
  <classRef>game.classifiers.single.DecisionTreeClassifier</classRef>
  <description>DecisionTree(maxdepth=20,conf=0.25,alt=10)</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
</configuration.classifiers.single.DecisionTreeClassifierConfig>"""

def heart_ensemble():
    return """<configuration.classifiers.single.KNNClassifierConfig>
  <classRef>game.classifiers.single.KNNClassifier</classRef>
  <description>KNN(k=15,vote=false,measure=CosineSimilarity)</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <nearestNeighbours>15</nearestNeighbours>
  <weightedVote>false</weightedVote>
  <measureType>
    <elements class="string-array">
      <string>EuclideanDistance</string>
      <string>CamberraDistance</string>
      <string>ChebychevDistance</string>
      <string>CorrelationSimilarity</string>
      <string>CosineSimilarity</string>
      <string>DiceSimilarity</string>
      <string>DynamicTimeWarpingDistance</string>
      <string>InnerProductSimilarity</string>
      <string>JaccardSimilarity</string>
      <string>ManhattanDistance</string>
      <string>MaxProductSimilarity</string>
      <string>OverlapSimilarity</string>
    </elements>
    <elementEnabled>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>true</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
      <boolean>false</boolean>
    </elementEnabled>
  </measureType>
</configuration.classifiers.single.KNNClassifierConfig>"""

def glass_ensemble():
    return """<configuration.classifiers.ensemble.ClassifierStackingProbConfig>
  <classRef>game.classifiers.ensemble.ClassifierStackingProb</classRef>
  <description>StackingProbabilities{4x KNN(k=2,vote=true,measure=ManhattanDistance)}</description>
  <maxLearningVectors>-1</maxLearningVectors>
  <maxInputsNumber>-1</maxInputsNumber>
  <classifiersNumber>4</classifiersNumber>
  <baseClassifiersDef>RANDOM</baseClassifiersDef>
  <baseClassifiersCfgs>
    <configuration.classifiers.single.KNNClassifierConfig>
      <classRef>game.classifiers.single.KNNClassifier</classRef>
      <maxLearningVectors>-1</maxLearningVectors>
      <maxInputsNumber>-1</maxInputsNumber>
      <nearestNeighbours>2</nearestNeighbours>
      <weightedVote>true</weightedVote>
      <measureType>
        <elements class="string-array">
          <string>EuclideanDistance</string>
          <string>CamberraDistance</string>
          <string>ChebychevDistance</string>
          <string>CorrelationSimilarity</string>
          <string>CosineSimilarity</string>
          <string>DiceSimilarity</string>
          <string>DynamicTimeWarpingDistance</string>
          <string>InnerProductSimilarity</string>
          <string>JaccardSimilarity</string>
          <string>ManhattanDistance</string>
          <string>MaxProductSimilarity</string>
          <string>OverlapSimilarity</string>
        </elements>
        <elementEnabled>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
          <boolean>true</boolean>
          <boolean>false</boolean>
          <boolean>false</boolean>
        </elementEnabled>
      </measureType>
    </configuration.classifiers.single.KNNClassifierConfig>
  </baseClassifiersCfgs>
</configuration.classifiers.ensemble.ClassifierStackingProbConfig>"""
