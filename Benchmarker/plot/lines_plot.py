import yaml
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import traceback
import numpy as np
from ..config import Names, Label
from ..utils import legend_label, label_metrics


class LinesPlot(yaml.YAMLObject):
    yaml_tag = u"Lines"

    def __init__(self, name ,title, xlab, ylab, x_col, y_col, style, legend_labels, group_by, log_x, screen):
        self.name = name
        self.title = title
        self.xlab = xlab
        self.ylab = ylab
        self.x_col = x_col
        self.y_col = y_col
        self.legend_labels = legend_labels
        self.group_by = group_by
        self.style = style
        self.log_x = log_x
        self.screen = screen

    def plot(self, file_path, data):
        res = data.groupby(self.group_by)
        i_max = len(res) + 1
        plt.figure()
        #plt.title(self.title)
        scaler = np.log if self.log_x else lambda x: x
        for screen in [True, False]:
            i = 1
            for lbl, g in res:
                #try:
                g = g.sort_values(self.x_col)
                label = self.legend_labels(lbl, g["name"].iloc[0])
                plt.plot(scaler(g[self.x_col]), g[self.y_col], self.style if "optimize" not in label else "^-",
                         c=sns.color_palette("Paired", n_colors=i_max)[i],  label=label)
                #except:
                #    print("An error occurred during ploting {}, {}, {}".format(self.x_col, self.y_col, lbl))
                #    traceback.print_tb(sys.exc_traceback)
                i += 1
        
            if screen:
                lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            else:
                lgd = plt.legend(bbox_to_anchor=(-0.0725, -0.125), ncol=2, loc=2, borderaxespad=0.)
            
            
            
            plt.xlabel(self.xlab)
            plt.ylabel(self.ylab)
      
            plt.savefig("{}/{}__x__{}{}_{}.pdf".format(file_path, self.x_col, self.y_col,
                                                       "_log_x" if self.log_x else "", 
                                                       "screen" if screen else "paper"),
                        bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.savefig("{}/{}__x__{}{}_{}.png".format(file_path, self.x_col, self.y_col,
                                                       "_log_x" if self.log_x else "", 
                                                       "screen" if screen else "paper"),
                        bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.close()

