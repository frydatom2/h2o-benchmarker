import yaml
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import traceback
import pandas as pd
from ..config import Names, Label
from ..utils import legend_label, label_metrics, legend_label_fg


class ViolinPlot(yaml.YAMLObject):
    yaml_tag = u"ViolinPlot"

    def __init__(self, name ,title, xlab, ylab, x_col, y_col, hue,  group_by, split, inner, scale, stripLabels, angle):
        self.name = name
        self.title = title
        self.xlab = xlab
        self.ylab = ylab
        self.x_col = x_col
        self.y_col = y_col
        self.group_by = group_by
        self.hue = hue
        self.split = split
        self.inner = inner
        self.scale = scale
        self.stripLabels = stripLabels
        self.angle = angle

    def plot(self, file_path, data):
        res = data.groupby(self.group_by)
        i_max = len(res) + 1
        plt.figure()
        #plt.title(self.title)
        i = 1
        for lbl, g in res:
            try:
                g = g.assign(generated_label = [legend_label_fg(x[[5, 6, 1, 2]], [x[3]]).split(",")[0] for i, x in g.iterrows()])
                g = g.sort_values(self.x_col)
                sns.violinplot(x=self.x_col, y=self.y_col, hue=self.hue, data=g, palette="Paired", split=self.split,
                               inner=self.inner, scale=self.scale, hue_order=sorted(set(g[self.hue])))
            except:
                print("An error occurred during ploting {}, {}, {}".format(self.x_col, self.y_col, lbl))
                traceback.print_tb(sys.exc_traceback)
                traceback.print_exc()
            i += 1

        plt.xlabel("")
        plt.ylabel(self.ylab)
        plt.setp(plt.axes().xaxis.get_ticklabels(), rotation=self.angle)
        if len(plt.legend().get_texts()) > 1:
            plt.legend(plt.legend().get_patches(),[str(x.get_text()).split("-")[1] for x in plt.legend().get_texts()]
                       if self.stripLabels else map(lambda x: x.get_text(), plt.legend().get_texts()),title="",loc='upper center')
        else:
            plt.legend().remove()
        plt.savefig("{}/{}__x__{}_{}_{}_violin.pdf".format(file_path, self.x_col, self.y_col, "split" if self.split else "notSplit", self.inner),bbox_inches='tight')
        plt.savefig("{}/{}__x__{}_{}_{}_violin.png".format(file_path, self.x_col, self.y_col, "split" if self.split else "notSplit", self.inner),bbox_inches='tight')
        plt.close()

