import yaml
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import traceback
import numpy as np
from ..config import Names, Label
from ..utils import legend_label_fg, label_metrics


class BarPlot(yaml.YAMLObject):
    yaml_tag = u"Bar"

    def __init__(self, name, title, xlab, ylab, x_col, y_col, screen):
        self.name = name
        self.title = title
        self.xlab = xlab
        self.ylab = ylab
        self.x_col = x_col
        self.y_col = y_col
        self.screen = screen

    def plot(self, file_path, data):
        plt.figure()
        subsets = list(data['subset'].unique()[:3])
        subsets.extend(data['subset'].unique()[3::8])
        subsets.append(data['subset'].unique()[-1])
        newdata = data[data['subset'].isin(subsets)]
        subsets = ["{:.1f} %".format(tick*100) for tick in sorted(subsets)]
        # plt.title(self.title)
        newdata["gen_label"] = newdata.apply(lambda dato: legend_label_fg([dato["model"], dato["params"]]), 1)
        for screen in [True, False]:
            sns.factorplot(x=self.x_col, y=self.y_col, hue="gen_label", data=newdata, kind="bar", legend_out=False,
                           aspect=2, ci=None,palette="Paired").set_xticklabels(subsets)
            if screen:
                lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            else:
                lgd = plt.legend(bbox_to_anchor=(-0.0725, -0.125), ncol=2, loc=2, borderaxespad=0.)

            plt.xlabel(self.xlab)
            plt.ylabel(self.ylab)

            plt.savefig("{}/{}__x__{}_{}_bars.pdf".format(file_path, self.x_col, self.y_col,
                                                       "screen" if screen else "paper"),
                        bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.savefig("{}/{}__x__{}_{}_bars.png".format(file_path, self.x_col, self.y_col,
                                                       "screen" if screen else "paper"),
                        bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.close()

