import yaml
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import traceback
from ..config import Names, Label
from ..utils import legend_label, label_metrics


class MinMaxLinesPlot(yaml.YAMLObject):
    yaml_tag = u"MinMaxLines"

    def __init__(self, name ,title, xlab, ylab, x_col, y_col, style, legend_labels, plot_points, group_by, minp, log_x):
        self.name = name
        self.title = title
        self.xlab = xlab
        self.ylab = ylab
        self.x_col = x_col
        self.y_col = y_col
        self.legend_labels = legend_labels
        self.group_by = group_by
        self.style = style
        self.plot_points = plot_points
        self.minp = minp
        self.log_x = log_x
        

    def getStyle(self, label, point=False):
        if point:
            if "SMAC" in label:
                return "^"
            else:
                return "o"
        if "SMAC" in label:
            return "--"
        return self.style

    def plot(self, file_path, data):
        res = data.groupby(self.group_by)
        i_max = len(res) + 1
        plt.figure()
        #plt.title(self.title)
        
        agg = min if self.minp else max
        scaler = np.log if self.log_x else lambda x: x
        for screen in [True, False]:
            i = 1
            for lbl, g in res:
                try:
    
                    g = g.sort_values(self.x_col)
    
                    label = self.legend_labels(lbl)
                    if not "Default Settings" in label:
                        plt.plot(scaler(g[self.x_col]), [agg(g[self.y_col][:max(nth+1,1)]) for nth in range(len(g[self.y_col]))],
                             self.getStyle(label), c=sns.color_palette("Paired", n_colors=i_max)[i],
                             label=label)
                        if self.plot_points:
                            plt.scatter(scaler(g[self.x_col]), g[self.y_col], marker=self.getStyle(label, True),
                                c=sns.color_palette("hls", n_colors=i_max)[i], alpha=.5, label="")
                    else:
                        plt.axhline(np.mean(g[self.y_col]), xmin = 0, linestyle=":", c = sns.color_palette("hls", n_colors=i_max)[i],
                                    label=label)
                except:
                    print("An error occurred during ploting {}, {}, {}".format(self.x_col, self.y_col, lbl))
                    traceback.print_tb(sys.exc_traceback)
                i += 1
    
            if screen:
                lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            else:
                lgd = plt.legend(bbox_to_anchor=(-0.0725, -0.125), ncol=2, loc=2, borderaxespad=0.)
    
            plt.xlabel(self.xlab)
            plt.ylabel(self.ylab)
    
            plt.savefig("{}/{}__x__{}_{}_{}_{}.pdf".format(file_path, self.x_col, self.y_col, "with_points" if self.plot_points else "without_points",
                                                           "log_scale" if self.log_x else "linear_scale", "screen" if screen else "paper"), 
                        bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.savefig("{}/{}__x__{}_{}_{}_{}.png".format(file_path, self.x_col, self.y_col, "with_points" if self.plot_points else "without_points",
                                                           "log_scale" if self.log_x else "linear_scale", "screen" if screen else "paper"),
                        bbox_extra_artists=(lgd,), bbox_inches='tight')
            plt.close()

